package com.example.data.dinjection

import com.example.data.repository.*
import com.example.domain.respository.*
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val repositoryModule = module {
    single<TvShowRepository> { TvShowRepositoryImpl(androidContext(), get(), get()) }
    single<SeasonRepository> { SeasonRepositoryImpl(androidContext(), get(), get()) }
    single<EpisodeRepository> { EpisodeRepositoryImpl(androidContext(), get(), get()) }

    single<FavoriteTvShowsRepository> { FavoriteTvShowsRepositoryImpl(androidContext(), get(), get()) }

    single<SessionRepository> { SessionRepositoryImpl(androidContext(), get()) }

    single<AccountRepository> { AccountRepositoryImpl(androidContext(), get()) }
}