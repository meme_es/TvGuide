package com.example.data.dinjection

import androidx.room.Room
import com.example.data.database.AppDatabase
import com.example.data.repository.datasourceinterface.episode.EpisodeLocalDatasource
import com.example.data.repository.datasourceinterface.favorite.FavoriteTvShowLocalDatasource
import com.example.data.repository.datasourceinterface.season.SeasonLocalDatasource
import com.example.data.repository.implementation.tvshow.TvShowLocalDatasourceImpl
import com.example.data.repository.datasourceinterface.tvshow.TvShowLocalDatasource
import com.example.data.repository.implementation.episode.EpisodeLocalDatasourceImpl
import com.example.data.repository.implementation.favorite.FavoriteTvShowLocalDatasourceImpl
import com.example.data.repository.implementation.season.SeasonLocalDatasourceImpl
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val databaseModule = module {
    single {
        Room.databaseBuilder(androidContext(), AppDatabase::class.java, "app_database").build()
    }

    single { get<AppDatabase>().tvShowDao() }
    single { get<AppDatabase>().seasonDao() }
    single { get<AppDatabase>().episodeDao() }

    single { get<AppDatabase>().favoriteDao() }

    single<TvShowLocalDatasource> { TvShowLocalDatasourceImpl(get()) }
    single<SeasonLocalDatasource> { SeasonLocalDatasourceImpl(get()) }
    single<EpisodeLocalDatasource> { EpisodeLocalDatasourceImpl(get()) }

    single<FavoriteTvShowLocalDatasource> { FavoriteTvShowLocalDatasourceImpl(get()) }
}