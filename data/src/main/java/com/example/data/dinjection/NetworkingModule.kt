package com.example.data.dinjection

import com.example.data.BuildConfig
import com.example.data.networking.service.RetrofitAppService
import com.example.data.repository.datasourceinterface.AccountRemoteDatasource
import com.example.data.repository.datasourceinterface.episode.EpisodeRemoteDatasource
import com.example.data.repository.datasourceinterface.season.SeasonRemoteDatasource
import com.example.data.repository.datasourceinterface.SessionRemoteDatasource
import com.example.data.repository.datasourceinterface.favorite.FavoriteTvShowRemoteDatasource
import com.example.data.repository.datasourceinterface.tvshow.TvShowRemoteDatasource
import com.example.data.repository.implementation.AccountRemoteDatasourceImpl
import com.example.data.repository.implementation.episode.EpisodeRemoteDatasourceImpl
import com.example.data.repository.implementation.season.SeasonRemoteDatasourceImpl
import com.example.data.repository.implementation.SessionRemoteDatasourceImpl
import com.example.data.repository.implementation.favorite.FavoriteTvShowRemoteDatasourceImpl
import com.example.data.repository.implementation.tvshow.TvShowRemoteDatasourceImpl
import org.koin.dsl.module
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val networkingModule = module {
    single { GsonConverterFactory.create() as Converter.Factory }

    single {
        Retrofit.Builder()
            .addConverterFactory(get())
            .baseUrl(BuildConfig.BASE_URL)
            .build()
    }

    single { get<Retrofit>().create(RetrofitAppService::class.java) }

    single<TvShowRemoteDatasource> { TvShowRemoteDatasourceImpl(get(), BuildConfig.API_KEY) }
    single<SeasonRemoteDatasource> { SeasonRemoteDatasourceImpl(get(), BuildConfig.API_KEY) }
    single<EpisodeRemoteDatasource> { EpisodeRemoteDatasourceImpl(get(), BuildConfig.API_KEY) }

    single<FavoriteTvShowRemoteDatasource> { FavoriteTvShowRemoteDatasourceImpl(get(), BuildConfig.API_KEY) }

    single<SessionRemoteDatasource> { SessionRemoteDatasourceImpl(get(), BuildConfig.API_KEY) }

    single<AccountRemoteDatasource> { AccountRemoteDatasourceImpl(get(), BuildConfig.API_KEY) }
}