package com.example.data.networking.model.account


import com.google.gson.annotations.SerializedName

data class TmdbResponse(
    @SerializedName("avatar_path")
    val avatarPath: String?
)