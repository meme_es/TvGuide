package com.example.data.networking.model.tvshow


import com.google.gson.annotations.SerializedName

data class TvShowResponseList(
    @SerializedName("results")
    val tvShows: List<TvShowResponse>?
)