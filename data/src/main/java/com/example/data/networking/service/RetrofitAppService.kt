package com.example.data.networking.service

import com.example.data.networking.model.episode.EpisodeResponseList
import com.example.data.networking.model.season.SeasonResponseList
import com.example.data.networking.model.session.SessionResponse
import com.example.data.networking.model.tvshow.TvShowResponseList
import com.example.data.networking.model.account.AccountResponse
import com.example.data.networking.model.favorite.FavoriteResponse
import com.example.domain.model.bodyrequest.FavoriteItem
import com.example.domain.model.bodyrequest.Login
import com.example.domain.model.bodyrequest.SessionId
import com.example.domain.model.bodyrequest.Token
import retrofit2.Response
import retrofit2.http.*

interface RetrofitAppService {
    @GET("tv/{type}")
    suspend fun getTvShows(
        @Path("type") type: String,
        @Query("api_key") apikey: String,
        @Query("page") page: String = "1"
    ): Response<TvShowResponseList>

    @GET("tv/{tv_id}")
    suspend fun getSeasonList(
        @Path("tv_id") tvShowId: Int,
        @Query("api_key") apikey: String
    ): Response<SeasonResponseList>

    @GET("tv/{tv_id}/season/{season_number}")
    suspend fun getEpisodeList(
        @Path("tv_id") tvShowId: Int,
        @Path("season_number") seasonNumber: Int,
        @Query("api_key") apikey: String
    ): Response<EpisodeResponseList>

    @GET("authentication/token/new")
    suspend fun requestToken(
        @Query("api_key") apikey: String
    ): Response<SessionResponse>

    @POST("authentication/token/validate_with_login")
    suspend fun authenticate(
        @Query("api_key") apikey: String,
        @Body login: Login
    ): Response<SessionResponse>

    @POST("authentication/session/new")
    suspend fun requestSession(
        @Query("api_key") apikey: String,
        @Body token: Token
    ): Response<SessionResponse>

    @GET("account")
    suspend fun getUserAccount(
        @Query("api_key") apikey: String,
        @Query("session_id") sessionId: String
    ): Response<AccountResponse>

    @GET("account/{account_id}/favorite/tv")
    suspend fun getUserFavoriteTvShows(
        @Path("account_id") accountId: Int,
        @Query("api_key") apikey: String,
        @Query("session_id") SessionId: String,
        @Query("page") page: String = "1"
    ): Response<TvShowResponseList>

    @POST("account/{account_id}/favorite")
    suspend fun addAsFavorite(
        @Path("account_id") accountId: Int,
        @Query("api_key") apikey: String,
        @Query("session_id") sessionId: String,
        @Body favorite: FavoriteItem
    ): Response<FavoriteResponse>

    @DELETE("authentication/session")
    suspend fun deleteSession(
        @Query("api_key") apikey: String,
        @Body sessionId: SessionId
    ): Response<SessionResponse>
}