package com.example.data.networking.model.account


import com.google.gson.annotations.SerializedName

data class AccountResponse(
    @SerializedName("id")
    val id: Int,
    @SerializedName("avatar")
    val avatar: AvatarResponse?,
    @SerializedName("name")
    val name: String?,
    @SerializedName("username")
    val username: String?
)