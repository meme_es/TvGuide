package com.example.data.networking.model.season

import com.google.gson.annotations.SerializedName

data class SeasonResponseList(
    @SerializedName("seasons")
    val tvShowSeasons: List<SeasonResponse>?,
)