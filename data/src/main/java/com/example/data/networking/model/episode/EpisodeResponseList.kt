package com.example.data.networking.model.episode


data class EpisodeResponseList(
    val episodes: List<EpisodeResponse>?
)