package com.example.data.networking.model.episode


import com.google.gson.annotations.SerializedName

data class EpisodeResponse(
    @SerializedName("id")
    val id: Int,
    @SerializedName("episode_number")
    val episodeNumber: Int?,
    @SerializedName("name")
    val name: String?,
    @SerializedName("still_path")
    val stillPath: String?,
    @SerializedName("vote_average")
    val voteAverage: Double?
)