package com.example.data.networking.model.account


import com.google.gson.annotations.SerializedName

data class AvatarResponse(
    @SerializedName("tmdb")
    val tmdb: TmdbResponse?
)