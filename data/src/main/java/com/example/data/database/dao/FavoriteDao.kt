package com.example.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.data.database.model.FavoriteEntity

@Dao
interface FavoriteDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveFavoriteTvShows(tvShows: List<FavoriteEntity>)

    @Query("DELETE FROM favorite_shows")
    suspend fun deleteFavoriteTvShows()

    @Query("SELECT * FROM favorite_shows")
    suspend fun getFavoriteTvShows(): List<FavoriteEntity>
}