package com.example.data.database.model

import androidx.room.ColumnInfo
import androidx.room.Entity

@Entity(tableName = "tv_shows", primaryKeys = ["id", "type"])
data class TvShowEntity(
    @ColumnInfo(name = "id")
    val id: Int,
    @ColumnInfo(name = "name")
    val name: String?,
    @ColumnInfo(name = "overview")
    val overview: String?,
    @ColumnInfo(name = "posterPath")
    val posterPath: String?,
    @ColumnInfo(name = "voteAverage")
    val voteAverage: Double?,
    @ColumnInfo(name = "type")
    val type: String
)