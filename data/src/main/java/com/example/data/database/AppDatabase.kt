package com.example.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.data.database.dao.EpisodeDao
import com.example.data.database.dao.FavoriteDao
import com.example.data.database.dao.SeasonDao
import com.example.data.database.dao.TvShowDao
import com.example.data.database.model.EpisodeEntity
import com.example.data.database.model.FavoriteEntity
import com.example.data.database.model.SeasonEntity
import com.example.data.database.model.TvShowEntity

@Database(entities = [TvShowEntity::class, SeasonEntity::class, EpisodeEntity::class, FavoriteEntity::class], version = 1, exportSchema = false)
abstract class AppDatabase: RoomDatabase() {
    abstract fun tvShowDao(): TvShowDao

    abstract fun seasonDao(): SeasonDao

    abstract fun episodeDao(): EpisodeDao

    abstract fun favoriteDao(): FavoriteDao
}