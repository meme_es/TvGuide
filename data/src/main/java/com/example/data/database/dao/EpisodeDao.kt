package com.example.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.data.database.model.EpisodeEntity

@Dao
interface EpisodeDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveEpisodes(episodes: List<EpisodeEntity>)

    @Query("DELETE FROM episodes WHERE tv_show_id = :tvShowId AND season_id = :seasonId")
    suspend fun deleteEpisodes(tvShowId: Int, seasonId: Int)

    @Query("SELECT * FROM episodes WHERE tv_show_id = :tvShowId AND season_id = :seasonId ORDER BY episode_number")
    suspend fun getEpisodes(tvShowId: Int, seasonId: Int): List<EpisodeEntity>
}