package com.example.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.data.database.model.SeasonEntity

@Dao
interface SeasonDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveSeasons(seasons: List<SeasonEntity>)

    @Query("DELETE FROM seasons WHERE tv_show_id = :tvShowId")
    suspend fun deleteSeasons(tvShowId: Int)

    @Query("SELECT * FROM seasons WHERE tv_show_id = :tvShowId ORDER BY season_number")
    suspend fun getSeasons(tvShowId: Int): List<SeasonEntity>
}