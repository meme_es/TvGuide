package com.example.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.data.database.model.TvShowEntity

@Dao
interface TvShowDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveTvShows(tvShows: List<TvShowEntity>)

    @Query("DELETE FROM tv_shows WHERE type = :type")
    suspend fun deleteTvShows(type: String)

    @Query("SELECT * FROM tv_shows WHERE type = :type")
    suspend fun getTvShows(type: String): List<TvShowEntity>
}