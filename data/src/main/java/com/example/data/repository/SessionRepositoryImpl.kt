package com.example.data.repository

import android.content.Context
import com.example.data.networking.model.session.SessionResponse
import com.example.data.repository.datasourceinterface.SessionRemoteDatasource
import com.example.domain.model.bodyrequest.Login
import com.example.domain.model.SessionResult
import com.example.domain.model.bodyrequest.Token
import com.example.domain.respository.SessionRepository
import com.google.gson.Gson

class SessionRepositoryImpl(
    private val context: Context,
    private val sessionRemoteDatasource: SessionRemoteDatasource
) : SessionRepository {
    override suspend fun requestSession(username: String, password: String): SessionResult {
        var session: SessionResult

        session = requestToken()
        if (session.success) {
            val login = Login(username, password, session.requestToken.toString())
            session = authenticate(login)
            if (session.success) {
                val token = Token(login.request_token)
                session = requestSessionId(token)
                session.requestToken = login.request_token
            }
        }

        return session
    }

    private suspend fun requestToken(): SessionResult {
        var session = SessionResult(false)

        try {
            val response = sessionRemoteDatasource.requestToken()
            if (response.isSuccessful) {
                val body = response.body()
                if (body != null) {
                    session = body.toSession()
                }
            }
        } catch (exception: Exception) {
            session.statusMessage = "Unable to request a token, check your Internet connexion"
            return session
        }

        return session
    }

    private suspend fun authenticate(login: Login): SessionResult {
        var session = SessionResult(false)

        try {
            val response = sessionRemoteDatasource.authenticate(login)
            if (response.isSuccessful) {
                val body = response.body()
                if (body != null) {
                    session = body.toSession()
                }
            }
            else if (response.errorBody() != null) {
                val errorBody = response.errorBody()
                val errorString = errorBody?.string()
                var gson = Gson()
                var sessionResponse = gson.fromJson(errorString, SessionResponse::class.java)
                session = sessionResponse.toSession()
            }
        } catch (exception: Exception) {
            session.statusMessage = "Unable to authenticate, check your Internet connexion"
            return session
        }

        return session
    }

    private suspend fun requestSessionId(token: Token): SessionResult {
        var session = SessionResult(false)

        try {
            val response = sessionRemoteDatasource.requestSession(token)
            if (response.isSuccessful) {
                val body = response.body()
                if (body != null) {
                    session = body.toSession()
                }
            }
        } catch (exception: Exception) {
            session.statusMessage = "Unable request a session, check your Internet connexion"
            return session
        }

        return session
    }

    private fun SessionResponse.toSession() = SessionResult(
        success = success,
        requestToken = requestToken,
        statusCode = statusCode,
        statusMessage = statusMessage,
        sessionId = sessionId
    )
}