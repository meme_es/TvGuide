package com.example.data.repository

import android.content.Context
import android.widget.Toast
import com.example.data.repository.datasourceinterface.AccountRemoteDatasource
import com.example.domain.model.Account
import com.example.domain.respository.AccountRepository

class AccountRepositoryImpl(
    private val context: Context,
    private val accountRemoteDatasource: AccountRemoteDatasource,
) : AccountRepository {
    override suspend fun getAccountInfo(sessionId: String): Account {
        var account = Account()

        try {
            val response = accountRemoteDatasource.getAccountInfo(sessionId)
            if (response.isSuccessful) {
                val body = response.body()
                if (body != null) {
                    account = Account(body.id, body.name, body.username, body.avatar?.tmdb?.avatarPath)
                }
            }
        } catch (exception: Exception) {
            Toast.makeText(context, "Unable to reach remote data for account", Toast.LENGTH_LONG).show()
            return account
        }

        return account
    }
}