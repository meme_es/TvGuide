package com.example.data.repository.implementation.episode

import com.example.data.networking.service.RetrofitAppService
import com.example.data.repository.datasourceinterface.episode.EpisodeRemoteDatasource

class EpisodeRemoteDatasourceImpl(
    private val retrofitAppService: RetrofitAppService,
    private val apiKey: String
) : EpisodeRemoteDatasource {
    override suspend fun getEpisodes(tvShowId: Int, seasonId: Int) = retrofitAppService.getEpisodeList(tvShowId, seasonId, apiKey)
}