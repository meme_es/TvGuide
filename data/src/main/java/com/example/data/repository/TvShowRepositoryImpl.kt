package com.example.data.repository

import android.content.Context
import android.widget.Toast
import com.example.data.database.model.TvShowEntity
import com.example.data.repository.datasourceinterface.tvshow.TvShowLocalDatasource
import com.example.data.repository.datasourceinterface.tvshow.TvShowRemoteDatasource
import com.example.domain.model.TvShow
import com.example.domain.respository.TvShowRepository

class TvShowRepositoryImpl(
    private val context: Context,
    private val tvShowRemoteDatasource: TvShowRemoteDatasource,
    private val tvShowLocalDatasource: TvShowLocalDatasource
) : TvShowRepository {
    override suspend fun getTvShows(type: String): List<TvShow> {
        getTvShowsFromAPI(type)
        return getTvShowsFromDB(type)
    }

    private suspend fun getTvShowsFromAPI(type: String) {
        try {
            val response = tvShowRemoteDatasource.getTvShows(type)
            val body = response.body()
            if (body?.tvShows != null) {
                val tvShowList = body.tvShows.map {
                    TvShowEntity(it.id, it.name, it.overview, it.posterPath, it.voteAverage, type = type)
                }

                tvShowLocalDatasource.saveTvShowsToDB(tvShowList)
            }
        } catch (exception: Exception) {
            Toast.makeText(context, "Unable to reach remote data", Toast.LENGTH_LONG).show()
        }
    }

    private suspend fun getTvShowsFromDB(type: String): List<TvShow> {
        var tvShowList = listOf<TvShow>()

        try {
            tvShowList = tvShowLocalDatasource.getTvShowsFromDB(type).map {
                TvShow(it.id, it.name, it.overview, it.posterPath, it.voteAverage)
            }
        } catch (exception: Exception) {
            Toast.makeText(context, "Unable to get data from Database", Toast.LENGTH_LONG).show()
        }

        return tvShowList
    }
}