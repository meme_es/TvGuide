package com.example.data.repository.implementation.favorite

import com.example.data.networking.model.favorite.FavoriteResponse
import com.example.data.networking.model.tvshow.TvShowResponseList
import com.example.data.networking.service.RetrofitAppService
import com.example.data.repository.datasourceinterface.favorite.FavoriteTvShowRemoteDatasource
import com.example.domain.model.bodyrequest.FavoriteItem
import retrofit2.Response

class FavoriteTvShowRemoteDatasourceImpl(
    private val retrofitAppService: RetrofitAppService,
    private val apiKey: String
) : FavoriteTvShowRemoteDatasource {
    override suspend fun getFavoriteTvShows(accountId: Int, sessionId: String): Response<TvShowResponseList> {
        return retrofitAppService.getUserFavoriteTvShows(accountId, apiKey, sessionId)
    }

    override suspend fun addFavoriteTVShow(
        accountId: Int,
        sessionId: String,
        favorite: FavoriteItem
    ): Response<FavoriteResponse> {
        return retrofitAppService.addAsFavorite(accountId, apiKey, sessionId, favorite)
    }
}