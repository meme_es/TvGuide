package com.example.data.repository.datasourceinterface.favorite

import com.example.data.database.model.FavoriteEntity

interface FavoriteTvShowLocalDatasource {
    suspend fun getFavoriteTvShowsFromDB(): List<FavoriteEntity>
    suspend fun saveFavoriteTvShowsToDB(tvShows: List<FavoriteEntity>)
    suspend fun clearFavorites()
}