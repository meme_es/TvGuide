package com.example.data.repository.implementation.episode

import com.example.data.database.dao.EpisodeDao
import com.example.data.database.model.EpisodeEntity
import com.example.data.repository.datasourceinterface.episode.EpisodeLocalDatasource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class EpisodeLocalDatasourceImpl(private val episodeDao: EpisodeDao) : EpisodeLocalDatasource {
    override suspend fun getEpisodesFromDB(tvShowId: Int, seasonId: Int) = episodeDao.getEpisodes(tvShowId, seasonId)

    override suspend fun saveEpisodesToDB(episodes: List<EpisodeEntity>) {
        CoroutineScope(Dispatchers.IO).launch {
            episodeDao.saveEpisodes(episodes)
        }
    }

    override suspend fun clearEpisodes(tvShowId: Int, seasonId: Int) {
        CoroutineScope(Dispatchers.IO).launch {
            episodeDao.deleteEpisodes(tvShowId, seasonId)
        }
    }
}