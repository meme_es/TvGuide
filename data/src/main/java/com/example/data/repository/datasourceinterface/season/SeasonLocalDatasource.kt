package com.example.data.repository.datasourceinterface.season

import com.example.data.database.model.SeasonEntity

interface SeasonLocalDatasource {
    suspend fun getSeasonsFromDB(tvShowId: Int): List<SeasonEntity>
    suspend fun saveSeasonsToDB(seasons: List<SeasonEntity>)
    suspend fun clearSeasons(tvShowId: Int)
}