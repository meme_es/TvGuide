package com.example.data.repository.datasourceinterface.episode

import com.example.data.database.model.EpisodeEntity

interface EpisodeLocalDatasource {
    suspend fun getEpisodesFromDB(tvShowId: Int, seasonId: Int): List<EpisodeEntity>
    suspend fun saveEpisodesToDB(episodes: List<EpisodeEntity>)
    suspend fun clearEpisodes(tvShowId: Int, seasonId: Int)
}