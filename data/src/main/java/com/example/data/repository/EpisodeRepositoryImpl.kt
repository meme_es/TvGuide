package com.example.data.repository

import android.content.Context
import android.widget.Toast
import com.example.data.database.model.EpisodeEntity
import com.example.data.repository.datasourceinterface.episode.EpisodeLocalDatasource
import com.example.data.repository.datasourceinterface.episode.EpisodeRemoteDatasource
import com.example.domain.model.Episode
import com.example.domain.respository.EpisodeRepository

class EpisodeRepositoryImpl(
    private val context: Context,
    private val episodeRemoteDatasource: EpisodeRemoteDatasource,
    private val episodeLocalDatasource: EpisodeLocalDatasource
) : EpisodeRepository {
    override suspend fun getSeasonEpisodes(tvShowId: Int, seasonId: Int, seasonNumber: Int): List<Episode> {
        getEpisodesFromAPI(tvShowId, seasonId, seasonNumber)
        return getEpisodesFromDB(tvShowId, seasonId)
    }

    private suspend fun getEpisodesFromAPI(tvShowId: Int, seasonId: Int, seasonNumber: Int) {
        try {
            val response = episodeRemoteDatasource.getEpisodes(tvShowId, seasonNumber)
            val body = response.body()
            if (body?.episodes != null) {
                val episodeList = body.episodes.map {
                    EpisodeEntity(it.id, tvShowId, seasonId, it.episodeNumber, it.name, it.stillPath, it.voteAverage)
                }

                episodeLocalDatasource.saveEpisodesToDB(episodeList)
            }
        } catch (exception: Exception) {
            Toast.makeText(context, "Unable to reach episodes from remote source", Toast.LENGTH_LONG).show()
        }
    }

    private suspend fun getEpisodesFromDB(tvShowId: Int, seasonId: Int): List<Episode> {
        var episodeList = listOf<Episode>()

        try {
            episodeList = episodeLocalDatasource.getEpisodesFromDB(tvShowId, seasonId).map {
                Episode(it.id, it.episodeNumber, it.name, it.stillPath, it.voteAverage)
            }
        } catch (exception: Exception) {
            Toast.makeText(context, "Unable to get episodes from Database", Toast.LENGTH_LONG).show()
        }

        return episodeList
    }
}