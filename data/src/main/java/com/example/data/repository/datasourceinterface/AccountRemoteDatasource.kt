package com.example.data.repository.datasourceinterface

import com.example.data.networking.model.account.AccountResponse
import retrofit2.Response

interface AccountRemoteDatasource {
    suspend fun getAccountInfo(sessionId: String): Response<AccountResponse>
}