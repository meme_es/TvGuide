package com.example.data.repository.datasourceinterface.favorite

import com.example.data.networking.model.favorite.FavoriteResponse
import com.example.data.networking.model.tvshow.TvShowResponseList
import com.example.domain.model.bodyrequest.FavoriteItem
import retrofit2.Response

interface FavoriteTvShowRemoteDatasource {
    suspend fun getFavoriteTvShows(accountId: Int, sessionId: String): Response<TvShowResponseList>
    suspend fun addFavoriteTVShow(accountId: Int, sessionId: String, favorite: FavoriteItem): Response<FavoriteResponse>
}