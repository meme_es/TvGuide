package com.example.data.repository

import android.content.Context
import android.widget.Toast
import com.example.data.database.model.FavoriteEntity
import com.example.data.repository.datasourceinterface.favorite.FavoriteTvShowLocalDatasource
import com.example.data.repository.datasourceinterface.favorite.FavoriteTvShowRemoteDatasource
import com.example.domain.model.FavoriteResult
import com.example.domain.model.TvShow
import com.example.domain.model.bodyrequest.FavoriteItem
import com.example.domain.respository.FavoriteTvShowsRepository

class FavoriteTvShowsRepositoryImpl(
    private val context: Context,
    private val favoriteTvShowRemoteDatasource: FavoriteTvShowRemoteDatasource,
    private val favoriteTvShowLocalDatasource: FavoriteTvShowLocalDatasource
) : FavoriteTvShowsRepository {
    override suspend fun getFavoriteTvShows(accountId: Int, sessionId: String): List<TvShow> {
        getFavoriteTvShowsFromAPI(accountId, sessionId)
        return getFavoriteTvShowsFromDB()
    }

    override suspend fun addFavoriteTvShow(
        accountId: Int,
        sessionId: String,
        favorite: FavoriteItem
    ): FavoriteResult {
        var favoriteResult = FavoriteResult()

        try {
            val response =  favoriteTvShowRemoteDatasource.addFavoriteTVShow(accountId, sessionId, favorite)
            if (response.isSuccessful) {
                val body = response.body()
                if (body != null) {
                    favoriteResult = FavoriteResult(body.statusCode, body.statusMessage)
                }
            }
        } catch (exception: Exception) {
            favoriteResult.statusMessage = "Unable to reach remote connexion, fail adding favorite TV show"
            return favoriteResult
        }

        return favoriteResult
    }

    private suspend fun getFavoriteTvShowsFromAPI(accountId: Int, sessionId: String) {
        try {
            val response = favoriteTvShowRemoteDatasource.getFavoriteTvShows(accountId, sessionId)
            val body = response.body()
            if (body?.tvShows != null) {
                val tvShowList = body.tvShows.map {
                    FavoriteEntity(it.id, it.name, it.overview, it.posterPath, it.voteAverage)
                }

                favoriteTvShowLocalDatasource.clearFavorites()
                favoriteTvShowLocalDatasource.saveFavoriteTvShowsToDB(tvShowList)
            }
        } catch (exception: Exception) {
            Toast.makeText(context, "Unable to reach remote data", Toast.LENGTH_LONG).show()
        }
    }

    private suspend fun getFavoriteTvShowsFromDB(): List<TvShow> {
        var tvShowList = listOf<TvShow>()

        try {
            tvShowList = favoriteTvShowLocalDatasource.getFavoriteTvShowsFromDB().map {
                TvShow(it.id, it.name, it.overview, it.posterPath, it.voteAverage)
            }
        } catch (exception: Exception) {
            Toast.makeText(context, "Unable to get data from Database", Toast.LENGTH_LONG).show()
        }

        return tvShowList
    }
}