package com.example.data.repository.implementation

import com.example.data.networking.model.account.AccountResponse
import com.example.data.networking.service.RetrofitAppService
import com.example.data.repository.datasourceinterface.AccountRemoteDatasource
import retrofit2.Response

class AccountRemoteDatasourceImpl(
    private val retrofitAppService: RetrofitAppService,
    private val apiKey: String
) : AccountRemoteDatasource {
    override suspend fun getAccountInfo(sessionId: String): Response<AccountResponse> {
        return retrofitAppService.getUserAccount(apiKey, sessionId)
    }
}