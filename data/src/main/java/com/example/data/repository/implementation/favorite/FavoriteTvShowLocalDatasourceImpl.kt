package com.example.data.repository.implementation.favorite

import com.example.data.database.dao.FavoriteDao
import com.example.data.database.model.FavoriteEntity
import com.example.data.repository.datasourceinterface.favorite.FavoriteTvShowLocalDatasource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class FavoriteTvShowLocalDatasourceImpl(private val favoriteDao: FavoriteDao): FavoriteTvShowLocalDatasource {
    override suspend fun getFavoriteTvShowsFromDB(): List<FavoriteEntity> = favoriteDao.getFavoriteTvShows()

    override suspend fun saveFavoriteTvShowsToDB(tvShows: List<FavoriteEntity>) {
        CoroutineScope(Dispatchers.IO).launch {
            favoriteDao.saveFavoriteTvShows(tvShows)
        }
    }

    override suspend fun clearFavorites() {
        CoroutineScope(Dispatchers.IO).launch {
            favoriteDao.deleteFavoriteTvShows()
        }
    }
}