package com.example.data.repository.datasourceinterface.season

import com.example.data.networking.model.season.SeasonResponseList
import retrofit2.Response

interface SeasonRemoteDatasource {
    suspend fun getSeasons(tvShowId: Int): Response<SeasonResponseList>
}