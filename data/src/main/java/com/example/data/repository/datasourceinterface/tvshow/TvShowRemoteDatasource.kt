package com.example.data.repository.datasourceinterface.tvshow

import com.example.data.networking.model.tvshow.TvShowResponseList
import retrofit2.Response

interface TvShowRemoteDatasource {
    suspend fun getTvShows(type: String): Response<TvShowResponseList>
}