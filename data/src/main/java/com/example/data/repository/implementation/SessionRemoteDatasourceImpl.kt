package com.example.data.repository.implementation

import com.example.data.networking.model.session.SessionResponse
import com.example.data.networking.service.RetrofitAppService
import com.example.data.repository.datasourceinterface.SessionRemoteDatasource
import com.example.domain.model.bodyrequest.Login
import com.example.domain.model.bodyrequest.Token
import retrofit2.Response

class SessionRemoteDatasourceImpl(
    private val retrofitAppService: RetrofitAppService,
    private val apiKey: String
) : SessionRemoteDatasource {
    override suspend fun requestToken(): Response<SessionResponse> {
        return retrofitAppService.requestToken(apiKey)
    }

    override suspend fun authenticate(login: Login): Response<SessionResponse> {
        return retrofitAppService.authenticate(apiKey, login)
    }

    override suspend fun requestSession(token: Token): Response<SessionResponse> {
        return retrofitAppService.requestSession(apiKey, token)
    }
}