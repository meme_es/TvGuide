package com.example.data.repository.implementation.tvshow

import com.example.data.database.dao.TvShowDao
import com.example.data.database.model.TvShowEntity
import com.example.data.repository.datasourceinterface.tvshow.TvShowLocalDatasource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class TvShowLocalDatasourceImpl(private val tvShowDao: TvShowDao): TvShowLocalDatasource {

    override suspend fun getTvShowsFromDB(type: String): List<TvShowEntity> = tvShowDao.getTvShows(type)

    override suspend fun saveTvShowsToDB(tvShows: List<TvShowEntity>) {
        CoroutineScope(Dispatchers.IO).launch {
            tvShowDao.saveTvShows(tvShows)
        }
    }

    override suspend fun clearTvShows(type: String) {
        CoroutineScope(Dispatchers.IO).launch {
            tvShowDao.deleteTvShows(type)
        }
    }
}