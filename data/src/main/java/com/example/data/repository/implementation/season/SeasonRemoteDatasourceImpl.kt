package com.example.data.repository.implementation.season

import com.example.data.networking.service.RetrofitAppService
import com.example.data.repository.datasourceinterface.season.SeasonRemoteDatasource

class SeasonRemoteDatasourceImpl(
    private val retrofitAppService: RetrofitAppService,
    private val apiKey: String
) : SeasonRemoteDatasource {
    override suspend fun getSeasons(tvShowId: Int) = retrofitAppService.getSeasonList(tvShowId, apiKey)
}