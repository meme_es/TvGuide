package com.example.data.repository.datasourceinterface.tvshow

import com.example.data.database.model.TvShowEntity

interface TvShowLocalDatasource {
    suspend fun getTvShowsFromDB(type: String): List<TvShowEntity>
    suspend fun saveTvShowsToDB(tvShows: List<TvShowEntity>)
    suspend fun clearTvShows(type: String)
}