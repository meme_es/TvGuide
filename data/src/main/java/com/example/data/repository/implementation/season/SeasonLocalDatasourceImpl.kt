package com.example.data.repository.implementation.season

import com.example.data.database.dao.SeasonDao
import com.example.data.database.model.SeasonEntity
import com.example.data.repository.datasourceinterface.season.SeasonLocalDatasource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SeasonLocalDatasourceImpl(private val seasonDao: SeasonDao) : SeasonLocalDatasource {
    override suspend fun getSeasonsFromDB(tvShowId: Int) = seasonDao.getSeasons(tvShowId)

    override suspend fun saveSeasonsToDB(seasons: List<SeasonEntity>) {
        CoroutineScope(Dispatchers.IO).launch {
            seasonDao.saveSeasons(seasons)
        }
    }

    override suspend fun clearSeasons(tvShowId: Int) {
        CoroutineScope(Dispatchers.IO).launch {
            seasonDao.deleteSeasons(tvShowId)
        }
    }
}