package com.example.data.repository.datasourceinterface.episode

import com.example.data.networking.model.episode.EpisodeResponseList
import retrofit2.Response

interface EpisodeRemoteDatasource {
    suspend fun getEpisodes(tvShowId: Int, seasonId: Int): Response<EpisodeResponseList>
}