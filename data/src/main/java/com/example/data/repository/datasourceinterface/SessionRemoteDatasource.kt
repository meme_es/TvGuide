package com.example.data.repository.datasourceinterface

import com.example.data.networking.model.session.SessionResponse
import com.example.domain.model.bodyrequest.Login
import com.example.domain.model.bodyrequest.Token
import retrofit2.Response

interface SessionRemoteDatasource {
    suspend fun requestToken(): Response<SessionResponse>
    suspend fun authenticate(login: Login): Response<SessionResponse>
    suspend fun requestSession(token: Token): Response<SessionResponse>
}