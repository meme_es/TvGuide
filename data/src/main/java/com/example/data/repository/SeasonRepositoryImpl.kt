package com.example.data.repository

import android.content.Context
import android.util.Log
import android.widget.Toast
import com.example.data.database.model.SeasonEntity
import com.example.data.repository.datasourceinterface.season.SeasonLocalDatasource
import com.example.data.repository.datasourceinterface.season.SeasonRemoteDatasource
import com.example.domain.model.Season
import com.example.domain.respository.SeasonRepository

class SeasonRepositoryImpl(
    private val context: Context,
    private val seasonRemoteDatasource: SeasonRemoteDatasource,
    private val seasonLocalDatasource: SeasonLocalDatasource
) : SeasonRepository {
    override suspend fun getTvShowSeasons(tvShowId: Int): List<Season> {
        getSeasonsFromAPI(tvShowId)
        return getSeasonsFromDB(tvShowId)
    }

    private suspend fun getSeasonsFromAPI(tvShowId: Int) {
        try {
            val response = seasonRemoteDatasource.getSeasons(tvShowId)
            val body = response.body()
            if (body?.tvShowSeasons != null) {
                val seasonList = body.tvShowSeasons.map {
                    SeasonEntity(it.id, tvShowId, it.episodeCount, it.name, it.overview, it.posterPath, it.seasonNumber)
                }

                seasonLocalDatasource.saveSeasonsToDB(seasonList)
            }
        } catch (exception: Exception) {
            Toast.makeText(context, "Unable to reach remote tv show details", Toast.LENGTH_LONG).show()
        }
    }

    private suspend fun getSeasonsFromDB(tvShowId: Int): List<Season> {
        var seasonList = listOf<Season>()

        try {
            seasonList = seasonLocalDatasource.getSeasonsFromDB(tvShowId).map {
                Season(it.id, it.tvShowId, it.episodeCount, it.name, it.overview, it.posterPath, it.seasonNumber)
            }
        } catch (exception: Exception) {
            Toast.makeText(context, "Unable to get tv show details from Database", Toast.LENGTH_LONG).show()
        }

        return seasonList
    }
}