package com.example.data.repository.implementation.tvshow

import com.example.data.networking.model.tvshow.TvShowResponseList
import com.example.data.networking.service.RetrofitAppService
import com.example.data.repository.datasourceinterface.tvshow.TvShowRemoteDatasource
import retrofit2.Response

class TvShowRemoteDatasourceImpl(
    private val retrofitAppService: RetrofitAppService,
    private val apiKey: String
) : TvShowRemoteDatasource {
    override suspend fun getTvShows(type: String): Response<TvShowResponseList> = retrofitAppService.getTvShows(type, apiKey)
}