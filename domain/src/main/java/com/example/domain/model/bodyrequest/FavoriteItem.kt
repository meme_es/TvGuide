package com.example.domain.model.bodyrequest

data class FavoriteItem(
    val media_type: String = "tv",
    val media_id: Int,
    val favorite: Boolean = true
)
