package com.example.domain.model.bodyrequest

data class Token(
    val request_token: String
)