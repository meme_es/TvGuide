package com.example.domain.model.bodyrequest

data class SessionId(
    val session_id: String
)
