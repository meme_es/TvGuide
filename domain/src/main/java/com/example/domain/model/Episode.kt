package com.example.domain.model

data class Episode(
    val id: Int,
    val episodeNumber: Int?,
    val name: String?,
    val stillPath: String?,
    val voteAverage: Double?
)