package com.example.domain.model

import java.io.Serializable

data class TvShow(
    val id: Int,
    val name: String?,
    val overview: String?,
    val posterPath: String?,
    val voteAverage: Double?
) : Serializable
