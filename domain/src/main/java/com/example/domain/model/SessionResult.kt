package com.example.domain.model

data class SessionResult(
    val success: Boolean,
    var requestToken: String? = null,
    val statusCode: Int? = null,
    var statusMessage: String? = null,
    val sessionId: String? = null
)
