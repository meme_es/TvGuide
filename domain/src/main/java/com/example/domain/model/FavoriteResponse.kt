package com.example.domain.model

data class FavoriteResult(
    var statusCode: Int = 0,
    var statusMessage: String = ""
)
