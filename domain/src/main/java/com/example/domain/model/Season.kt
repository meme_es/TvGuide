package com.example.domain.model

import java.io.Serializable

data class Season(
    val id: Int,
    val tvShowId: Int,
    val episodeCount: Int?,
    val name: String?,
    val overview: String?,
    val posterPath: String?,
    val seasonNumber: Int
) : Serializable
