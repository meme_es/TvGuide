package com.example.domain.model

data class Session(
    val token: String = "",
    val sessionId: String = "",
    val userId: Int = 0,
    val active: Boolean = false
)
