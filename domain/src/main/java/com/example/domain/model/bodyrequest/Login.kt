package com.example.domain.model.bodyrequest

data class Login(
    val username: String,
    val password: String,
    val request_token: String
)
