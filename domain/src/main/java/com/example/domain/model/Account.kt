package com.example.domain.model

data class Account(
    val id: Int = 0,
    val name: String? = null,
    val username: String? = null,
    val avatarPath: String? = null
)
