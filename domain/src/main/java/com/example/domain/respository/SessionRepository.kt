package com.example.domain.respository

import com.example.domain.model.SessionResult

interface SessionRepository {
    suspend fun requestSession(username: String, password: String): SessionResult
}