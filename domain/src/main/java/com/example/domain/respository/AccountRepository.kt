package com.example.domain.respository

import com.example.domain.model.Account

interface AccountRepository {
    suspend fun getAccountInfo(sessionId: String): Account
}
