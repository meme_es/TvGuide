package com.example.domain.respository

import com.example.domain.model.Season

interface SeasonRepository {
    suspend fun getTvShowSeasons(tvShowId: Int): List<Season>
}