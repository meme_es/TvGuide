package com.example.domain.respository

import com.example.domain.model.FavoriteResult
import com.example.domain.model.TvShow
import com.example.domain.model.bodyrequest.FavoriteItem

interface FavoriteTvShowsRepository {
    suspend fun getFavoriteTvShows(accountId: Int, sessionId: String): List<TvShow>
    suspend fun addFavoriteTvShow(accountId: Int, sessionId: String, favorite: FavoriteItem): FavoriteResult
}