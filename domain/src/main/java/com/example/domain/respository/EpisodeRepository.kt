package com.example.domain.respository

import com.example.domain.model.Episode

interface EpisodeRepository {
    suspend fun getSeasonEpisodes(tvShowId: Int, seasonId: Int, seasonNumber: Int): List<Episode>
}