package com.example.domain.respository

import com.example.domain.model.TvShow

interface TvShowRepository {
    suspend fun getTvShows(type: String): List<TvShow>
}