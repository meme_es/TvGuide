package com.example.domain.interaction

import com.example.domain.model.bodyrequest.FavoriteItem
import com.example.domain.respository.FavoriteTvShowsRepository

class AddFavoriteTvShowUseCase(private val favoriteTvShowsRepository: FavoriteTvShowsRepository) {
    suspend fun execute(
        accountId: Int,
        sessionId: String,
        favorite: FavoriteItem
    ) = favoriteTvShowsRepository.addFavoriteTvShow(accountId, sessionId, favorite)
}