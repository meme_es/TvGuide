package com.example.domain.interaction

import com.example.domain.model.Season
import com.example.domain.respository.SeasonRepository

class GetTvShowSeasonsUseCase(private val seasonRepository: SeasonRepository) {
    suspend fun execute(tvShowId: Int): List<Season> = seasonRepository.getTvShowSeasons(tvShowId)
}