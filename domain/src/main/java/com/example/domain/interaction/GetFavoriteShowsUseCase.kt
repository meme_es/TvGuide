package com.example.domain.interaction

import com.example.domain.model.TvShow
import com.example.domain.respository.FavoriteTvShowsRepository

class GetFavoriteShowsUseCase(private val favoriteTvShowsRepository: FavoriteTvShowsRepository) {
    suspend fun execute(
        accountId: Int,
        sessionId: String
    ): List<TvShow> = favoriteTvShowsRepository.getFavoriteTvShows(accountId, sessionId)
}