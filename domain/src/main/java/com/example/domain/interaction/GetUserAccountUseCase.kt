package com.example.domain.interaction

import com.example.domain.model.Account
import com.example.domain.respository.AccountRepository

class GetUserAccountUseCase(private val accountRepository: AccountRepository) {
    suspend fun execute(sessionId: String): Account = accountRepository.getAccountInfo(sessionId)
}