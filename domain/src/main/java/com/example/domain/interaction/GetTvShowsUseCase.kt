package com.example.domain.interaction

import com.example.domain.model.TvShow
import com.example.domain.respository.TvShowRepository

class GetTvShowsUseCase(
    private val tvShowRepository: TvShowRepository
) {
    suspend fun execute(type: String): List<TvShow> = tvShowRepository.getTvShows(type)
}