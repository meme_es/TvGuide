package com.example.domain.interaction

import com.example.domain.model.SessionResult
import com.example.domain.respository.SessionRepository

class StartSessionUseCase(
    private val sessionRepository: SessionRepository
) {
    suspend fun execute(username: String, password: String): SessionResult = sessionRepository.requestSession(username, password)
}