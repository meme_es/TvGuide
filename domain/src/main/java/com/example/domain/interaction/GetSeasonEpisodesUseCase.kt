package com.example.domain.interaction

import com.example.domain.model.Episode
import com.example.domain.respository.EpisodeRepository

class GetSeasonEpisodesUseCase(private val episodeRepository: EpisodeRepository) {
    suspend fun execute(
        tvShowId: Int,
        seasonId: Int,
        seasonNumber: Int
    ): List<Episode> = episodeRepository.getSeasonEpisodes(tvShowId, seasonId, seasonNumber)
}