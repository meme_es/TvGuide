package com.example.domain.dinjection

import com.example.domain.interaction.*
import org.koin.dsl.module

val interactionModule = module {
    single { GetTvShowsUseCase(get()) }
    single { GetTvShowSeasonsUseCase(get()) }
    single { GetSeasonEpisodesUseCase(get()) }

    single { GetFavoriteShowsUseCase(get()) }
    single { AddFavoriteTvShowUseCase(get()) }

    single { StartSessionUseCase(get()) }

    single { GetUserAccountUseCase(get()) }
}