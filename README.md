
# TV Guide

> Android App that shows you a list of TV by category like by popularity, top-rated, etc.

This app lets you get TV show information, you can see details of a TV show like seasons, episodes by season, add a TV show as a favorite, and check your information account and your favorite TV show list.

![Screenshot](./details.png)

This app gets the information from the `https://api.themoviedb.org/3/` service API.

## Built With

- Kotlin
- Android Studio

## Getting Started

Get a local copy running the following command.

`git clone git@gitlab.com:meme_es/TvGuide.git`

### Prerequisites

- Android Studio

### Usage

Open the project using Android Studio, select an Android Virtual Device (AVD), press the run triangle(Run) button, or just press shift-10, and let the IDE make the rest.

To the TV shows categories available you must login.

![Screenshot](./login.png)

After login, you can si the TV show filters by popular, top-rated, and on air.

![Screenshot](./tvshows.png)

If you select a TV show, you can see its details, a list of seasons, and a list of episodes by season.

![Screenshot](./episodes.png)

In addition, as a user, you can see your account information like name, username, your avatar, and your TV show favorite list if you tap the user icon in the TV show details screen.

![Screenshot](./profile.png)

## Authors

👤 **Meme**

- GitLab: [@meme_es](https://gitlab.com/meme_es)
- Linkedin: [linkedin](https://www.linkedin.com/in/manuel-elias/)

## 🤝 Contributing

Contributions, issues, and feature requests are welcome!

Feel free to check the [issues page](https://gitlab.com/meme_es/TvGuide/-/issues).

## Show your support

Give a ⭐️ if you like this project!


## 📝 License

This project is unlicensed.
