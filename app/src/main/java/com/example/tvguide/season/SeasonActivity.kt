package com.example.tvguide.season

import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import com.example.domain.model.TvShow
import com.example.tvguide.utils.Utils
import com.example.tvguide.ui.compose.season.ComposeTvShowDetails
import com.example.tvguide.episode.EpisodeActivity
import com.example.tvguide.ui.theme.TvGuideTheme
import org.koin.androidx.viewmodel.ext.android.viewModel

class SeasonActivity : ComponentActivity() {
    
    private val seasonViewModel: SeasonViewModel by viewModel()
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val tvShow = intent.getSerializableExtra(Utils.EXTRA_TV_SHOW_OBJECT) as TvShow

        setContent {
            TvGuideTheme {
                Surface(color = MaterialTheme.colors.background) {
                    ComposeTvShowDetails(seasonViewModel, tvShow) {
                        val intent = Intent(this, EpisodeActivity::class.java).apply {
                            putExtra(Utils.EXTRA_SEASON_OBJECT, it)
                        }

                        startActivity(intent)
                    }
                }
            }
        }
    }
}