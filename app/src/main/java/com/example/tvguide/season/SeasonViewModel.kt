package com.example.tvguide.season

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.domain.interaction.AddFavoriteTvShowUseCase
import com.example.domain.interaction.GetTvShowSeasonsUseCase
import com.example.domain.model.Season
import com.example.domain.model.bodyrequest.FavoriteItem
import kotlinx.coroutines.launch

class SeasonViewModel(
    private val getTvShowSeasonsUseCase: GetTvShowSeasonsUseCase,
    private val addFavoriteTvShowUseCase: AddFavoriteTvShowUseCase
) : ViewModel() {
    val tvShowSeasonList: MutableLiveData<List<Season>> = MutableLiveData()

    fun getTvShowSeasons(tvShowId: Int) {
        viewModelScope.launch {
            val response = getTvShowSeasonsUseCase.execute(tvShowId)
            tvShowSeasonList.value = response
        }
    }

    fun addFavoriteTvShow(accountId: Int, sessionId: String, favorite: FavoriteItem) {
        viewModelScope.launch {
            val response = addFavoriteTvShowUseCase.execute(accountId, sessionId, favorite)
        }
    }
}