package com.example.tvguide.utils

object Utils {
    const val EXTRA_TV_SHOW_OBJECT = "com.example.tvguide.TV_SHOW_OBJ"
    const val EXTRA_SEASON_OBJECT = "com.example.tvguide.SEASON_OBJ"
    const val IMAGE_BASE_URL = "https://image.tmdb.org/t/p/w500"

    val title = listOf("Popular", "Top Rated", "On Tv", "Airing Today")
}