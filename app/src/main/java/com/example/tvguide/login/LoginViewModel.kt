package com.example.tvguide.login

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.example.domain.interaction.StartSessionUseCase

class LoginViewModel(
    private val startSessionUseCase: StartSessionUseCase
) : ViewModel() {
    fun requestSession(username: String, password: String) = liveData {
        val response = startSessionUseCase.execute(username, password)
        emit(response)
    }
}