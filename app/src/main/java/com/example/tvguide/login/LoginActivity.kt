package com.example.tvguide.login

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.example.domain.model.Session
import com.example.tvguide.R
import com.example.tvguide.TvGuideApp
import com.example.tvguide.databinding.ActivityLoginBinding
import com.example.tvguide.profile.ProfileViewModel
import com.example.tvguide.tvshow.TvShowActivity
import org.koin.androidx.viewmodel.ext.android.viewModel

class LoginActivity : AppCompatActivity() {

    private val loginViewModel: LoginViewModel by viewModel()
    private val profileViewModel: ProfileViewModel by viewModel()

    private lateinit var binding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.Theme_TvGuide_NoActionBar)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)

        val progressBar = binding.progressBar

        binding.loginButton.setOnClickListener {
            progressBar.visibility = View.VISIBLE
            val username = binding.usernameEditText.text.toString()
            if (!TextUtils.isEmpty(username)) {
                val password = binding.passwordEditText.text.toString()
                if (!TextUtils.isEmpty(password)){
                    loginViewModel.requestSession(username, password).observe(this, { request ->
                        if (request.success) {
                            val token = request.requestToken
                            val sessionId = request.sessionId
                            if (token != null && sessionId != null) {
                                profileViewModel.getUserAccountInfo(sessionId).observe(this, { account ->

                                    if (account.id != 0) {
                                        // start a new session
                                        TvGuideApp.session = Session(token, sessionId, account.id, request.success)
                                        val intent = Intent(this, TvShowActivity::class.java)
                                        ContextCompat.startActivity(this, intent, null)
                                    }
                                })
                            }
                        }
                        else {
                            progressBar.visibility = View.INVISIBLE
                            Toast.makeText(this, request.statusMessage, Toast.LENGTH_LONG).show()
                        }
                    })
                }
                else {
                    Toast.makeText(this, "You must provide your password", Toast.LENGTH_LONG).show()
                }
            }
            else {
                Toast.makeText(this, "You must provide a username", Toast.LENGTH_LONG).show()
            }
        }

    }

    override fun onResume() {
        super.onResume()
        val progressBar = binding.progressBar
        progressBar.visibility = View.INVISIBLE

        val usernameEditText = binding.usernameEditText
        usernameEditText.text.clear()

        val passwordEditText = binding.passwordEditText
        passwordEditText.text.clear()
        // set empty session
        TvGuideApp.session = Session()
    }
}