package com.example.tvguide.dinjection

import com.example.tvguide.login.LoginViewModel
import com.example.tvguide.tvshow.TvShowViewModel
import com.example.tvguide.episode.EpisodeViewModel
import com.example.tvguide.profile.ProfileViewModel
import com.example.tvguide.season.SeasonViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {
    viewModel { TvShowViewModel(get()) }
    viewModel { SeasonViewModel(get(), get()) }
    viewModel { EpisodeViewModel(get()) }

    viewModel { LoginViewModel(get()) }

    viewModel { ProfileViewModel(get(), get()) }
}