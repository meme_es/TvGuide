package com.example.tvguide.episode

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.domain.interaction.GetSeasonEpisodesUseCase
import com.example.domain.model.Episode
import kotlinx.coroutines.launch

class EpisodeViewModel(
    private val getSeasonEpisodesUseCase : GetSeasonEpisodesUseCase
) : ViewModel() {
    val seasonEpisodeList: MutableLiveData<List<Episode>> = MutableLiveData()

    fun getSeasonEpisodes(tvShowId: Int, seasonId: Int, seasonNumber: Int) {
        viewModelScope.launch {
            val response = getSeasonEpisodesUseCase.execute(tvShowId, seasonId, seasonNumber)
            seasonEpisodeList.value = response
        }
    }
}