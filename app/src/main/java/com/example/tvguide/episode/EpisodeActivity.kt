package com.example.tvguide.episode

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import com.example.domain.model.Season
import com.example.tvguide.utils.Utils
import com.example.tvguide.ui.compose.episode.ComposeEpisodeList
import com.example.tvguide.ui.theme.TvGuideTheme
import org.koin.androidx.viewmodel.ext.android.viewModel

class EpisodeActivity : ComponentActivity() {

    private val episodeViewModel: EpisodeViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val season = intent.getSerializableExtra(Utils.EXTRA_SEASON_OBJECT) as Season

        setContent {
            TvGuideTheme {
                Surface(color = MaterialTheme.colors.background) {
                    ComposeEpisodeList(episodeViewModel, season.tvShowId, season.id, season.seasonNumber)
                }
            }
        }
    }
}
