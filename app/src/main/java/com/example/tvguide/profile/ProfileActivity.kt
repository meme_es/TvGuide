package com.example.tvguide.profile

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import com.example.tvguide.R
import com.example.tvguide.TvGuideApp
import com.example.tvguide.ui.compose.profile.ComposeUserProfile
import com.example.tvguide.ui.theme.TvGuideTheme
import org.koin.androidx.viewmodel.ext.android.viewModel

class ProfileActivity : ComponentActivity() {

    private val profileViewModel: ProfileViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.Theme_TvGuide_NoActionBar)
        setContent {
            TvGuideTheme {
                ComposeUserProfile(
                    profileViewModel,
                    sessionId = TvGuideApp.session.sessionId,
                    accountId = TvGuideApp.session.userId
                )
            }
        }
    }
}