package com.example.tvguide.profile

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.example.domain.interaction.GetFavoriteShowsUseCase
import com.example.domain.interaction.GetUserAccountUseCase
import com.example.domain.model.TvShow
import kotlinx.coroutines.launch

class ProfileViewModel(
    private val getUserAccountUseCase: GetUserAccountUseCase,
    private val getFavoriteShowsUseCase: GetFavoriteShowsUseCase
) : ViewModel() {
    val tvShowList: MutableLiveData<List<TvShow>> = MutableLiveData()

    fun getFavoriteTvShows(accountId: Int, sessionId: String) {
        viewModelScope.launch {
            val response = getFavoriteShowsUseCase.execute(accountId, sessionId)
            tvShowList.value = response
        }
    }

    fun getUserAccountInfo(sessionId: String) = liveData {
        val response = getUserAccountUseCase.execute(sessionId)
        emit(response)
    }
}