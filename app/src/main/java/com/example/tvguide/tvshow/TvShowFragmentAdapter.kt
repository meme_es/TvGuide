package com.example.tvguide.tvshow

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter

class TvShowFragmentAdapter(
    fragmentActivity: FragmentActivity
) : FragmentStateAdapter(fragmentActivity) {

    override fun getItemCount() = 4

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> TvShowPlaceholderFragment("popular")
            1 -> TvShowPlaceholderFragment("top_rated")
            2 -> TvShowPlaceholderFragment("on_the_air")
            else -> TvShowPlaceholderFragment("airing_today")
        }
    }
}