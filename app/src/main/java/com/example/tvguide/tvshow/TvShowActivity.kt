package com.example.tvguide.tvshow

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.example.tvguide.R
import com.example.tvguide.utils.Utils
import com.example.tvguide.databinding.ActivityTvShowBinding
import com.example.tvguide.profile.ProfileActivity
import com.google.android.material.tabs.TabLayoutMediator

class TvShowActivity : AppCompatActivity() {

    private lateinit var binding: ActivityTvShowBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.Theme_TvGuide_NoActionBar)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_tv_show)

        val userIcon = binding.userImageView
        userIcon.setOnClickListener {
            val intent = Intent(this, ProfileActivity::class.java)
            ContextCompat.startActivity(this, intent, null)
        }

        val tabLayout = binding.tabLayout
        val viewPager = binding.viewPager

        viewPager.adapter = TvShowFragmentAdapter(this)
        TabLayoutMediator(tabLayout, viewPager) { tab, position ->
            tab.text = Utils.title[position]
        }.attach()
    }
}