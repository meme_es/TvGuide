package com.example.tvguide.tvshow

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.domain.interaction.GetTvShowsUseCase
import com.example.domain.model.TvShow
import kotlinx.coroutines.launch

class TvShowViewModel(
    private val getTvShowsUseCase: GetTvShowsUseCase
) : ViewModel() {
    val tvShowsResponse: MutableLiveData<List<TvShow>> = MutableLiveData()

    fun getTvShows(type: String) {
        viewModelScope.launch {
            val response = getTvShowsUseCase.execute(type)
            tvShowsResponse.value = response
        }
    }
}