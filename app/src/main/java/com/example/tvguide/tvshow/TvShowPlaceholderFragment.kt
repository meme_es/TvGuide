package com.example.tvguide.tvshow

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.example.tvguide.databinding.FragmentTvShowsBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class TvShowPlaceholderFragment(private val type: String) : Fragment() {

    private val tvShowViewModel: TvShowViewModel by viewModel()
    private lateinit var binding: FragmentTvShowsBinding
    private lateinit var recyclerViewAdapter: TvShowRecyclerViewAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentTvShowsBinding.inflate(inflater, container, false)
        val root = binding.root

        binding.recyclerView.layoutManager = GridLayoutManager(activity, 2)
        recyclerViewAdapter = TvShowRecyclerViewAdapter()
        binding.recyclerView.adapter = recyclerViewAdapter

        tvShowViewModel.getTvShows(type)
        tvShowViewModel.tvShowsResponse.observe(viewLifecycleOwner, { list ->
            if (list != null) {
                with(recyclerViewAdapter) {
                    setList(list)
                    notifyDataSetChanged()
                }
            }
            else {
                Toast.makeText(activity, "No data available in DB", Toast.LENGTH_LONG).show()
            }
        })

        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding.unbind()
    }
}