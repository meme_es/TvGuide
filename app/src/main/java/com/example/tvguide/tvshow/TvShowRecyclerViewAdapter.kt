package com.example.tvguide.tvshow

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat.startActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.domain.model.TvShow
import com.example.tvguide.R
import com.example.tvguide.utils.Utils
import com.example.tvguide.databinding.ItemTvShowBinding
import com.example.tvguide.season.SeasonActivity
import kotlin.math.roundToInt

class TvShowRecyclerViewAdapter : RecyclerView.Adapter<TvShowRecyclerViewAdapter.ViewHolder>() {
    private val tvShows = ArrayList<TvShow>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: ItemTvShowBinding = DataBindingUtil.inflate(
            layoutInflater,
            R.layout.item_tv_show,
            parent,
            false
        )

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(tvShows[position])
    }

    override fun getItemCount() = tvShows.size

    fun setList(tvShowList: List<TvShow>) {
        tvShows.clear()
        tvShows.addAll(tvShowList)
    }

    class ViewHolder(private val binding: ItemTvShowBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(tvShow: TvShow) {
            binding.nameTextView.text = tvShow.name
            binding.pathTextView.text = tvShow.voteAverage?.minus(0.05)?.roundToInt()?.div(2.0).toString()

            val posterURL = Utils.IMAGE_BASE_URL + tvShow.posterPath
            Glide.with(binding.imageView.context)
                .load(posterURL)
                .into(binding.imageView)

            itemView.setOnClickListener {
                val intent = Intent(itemView.context, SeasonActivity::class.java).apply {
                    putExtra(Utils.EXTRA_TV_SHOW_OBJECT, tvShow)
                }

                startActivity(itemView.context, intent, null)
            }
        }
    }
}