package com.example.tvguide.ui.compose.episode

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.unit.dp
import com.example.domain.model.Episode
import com.example.tvguide.episode.EpisodeViewModel

@Composable
fun ComposeEpisodeList(episodeViewModel: EpisodeViewModel, tvShowId: Int, seasonId: Int, seasonNumber: Int) {
    episodeViewModel.getSeasonEpisodes(tvShowId, seasonId, seasonNumber)
    val episodeList: List<Episode> by episodeViewModel.seasonEpisodeList.observeAsState(listOf())

    Column {
        TopAppBar(
            title = {
                Text(text = "Season $seasonNumber")
            }
        )

        LazyColumn(
            contentPadding = PaddingValues(8.dp)
        ) {
            items(
                items = episodeList,
                itemContent = {
                    EpisodeItem(episodeItem = it)
                }
            )
        }
    }
}