package com.example.tvguide.ui.compose.profile

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.ContentAlpha
import androidx.compose.material.LocalContentAlpha
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import coil.compose.rememberImagePainter
import com.example.domain.model.TvShow
import com.example.tvguide.R
import com.example.tvguide.utils.Utils
import kotlin.math.roundToInt

@Composable
fun TvShowItem(tvShow: TvShow) {
    Card(
        modifier = Modifier
            .padding(8.dp),
        elevation = 8.dp,
        shape = RoundedCornerShape(corner = CornerSize(10.dp))
    ) {
        Column(
            modifier = Modifier.padding(bottom = 16.dp)
        ) {
            Image(
                painter = if (tvShow.posterPath != null) rememberImagePainter(Utils.IMAGE_BASE_URL + tvShow.posterPath) else painterResource(id = R.drawable.no_image_camera),
                contentDescription = stringResource(R.string.season_thumbnail),
                contentScale = ContentScale.FillWidth,
                modifier = Modifier
                    .height(200.dp)
                    .width(200.dp)
            )

            Spacer(modifier = Modifier.height(4.dp))

            CompositionLocalProvider(LocalContentAlpha provides ContentAlpha.medium) {
                tvShow.name?.let {
                    Text(
                        text = it,
                        modifier = Modifier.padding(horizontal = 16.dp)
                    )
                }

                tvShow.voteAverage?.let {
                    Text(
                        text = it?.minus(0.05)?.roundToInt()?.div(2.0).toString(),
                        modifier = Modifier.padding(horizontal = 16.dp)
                    )
                }
            }
        }
    }
}