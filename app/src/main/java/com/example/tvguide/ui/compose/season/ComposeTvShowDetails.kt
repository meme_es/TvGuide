package com.example.tvguide.ui.compose.season

import android.widget.Toast
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.platform.LocalContext
import com.example.domain.model.Season
import com.example.domain.model.TvShow
import com.example.domain.model.bodyrequest.FavoriteItem
import com.example.tvguide.TvGuideApp
import com.example.tvguide.season.SeasonViewModel
import com.example.tvguide.ui.compose.profile.TvShowCover

@Composable
fun ComposeTvShowDetails(seasonViewModel: SeasonViewModel, tvShow: TvShow, selectedItem: (Season) -> Unit) {
    seasonViewModel.getTvShowSeasons(tvShow.id)
    val seasonList: List<Season> by seasonViewModel.tvShowSeasonList.observeAsState(listOf())

    val context = LocalContext.current

    Column {
        TopAppBar(
            title = {
                Text(text = tvShow.name.toString())
            },
            actions = {
                IconButton(onClick = {
                    seasonViewModel.addFavoriteTvShow(
                        TvGuideApp.session.userId,
                        TvGuideApp.session.sessionId,
                        FavoriteItem(media_id = tvShow.id)
                    )
                    Toast.makeText(context, "TV show added as favorite", Toast.LENGTH_SHORT).show()
                }) {
                    Icon(Icons.Filled.Favorite, contentDescription = null)
                }
            }
        )

        LazyColumn {
            item {
                TvShowCover(tvShow)
            }

            items(
                items = seasonList,
                itemContent = {
                    SeasonItem(seasonItem = it, selectedItem)
                }
            )
        }
    }
}