package com.example.tvguide.ui.compose.profile

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.ContentAlpha
import androidx.compose.material.LocalContentAlpha
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberImagePainter
import com.example.domain.model.Account
import com.example.tvguide.R
import com.example.tvguide.utils.Utils

@Composable
fun UserProfile(account: Account) {
    Column(
        modifier = Modifier
            .padding(8.dp)
            .fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Image(
            painter = if (account.avatarPath != null) rememberImagePainter(Utils.IMAGE_BASE_URL + account.avatarPath ) else painterResource(id = R.drawable.no_image_camera),
            contentDescription = stringResource(R.string.avatar_image),
            contentScale = ContentScale.Crop,
            modifier = Modifier
                .padding(8.dp)
                .height(150.dp)
                .width(150.dp)
                .clip(CircleShape)
        )
        CompositionLocalProvider(LocalContentAlpha provides ContentAlpha.medium) {
            account.name?.let { Text(
                text = it,
                fontWeight = FontWeight.Bold
            ) }
            account.username?.let { Text(
                text = "@$it",
                fontSize = 12.sp
            ) }
        }
    }
}