package com.example.tvguide.ui.compose.profile

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberImagePainter
import com.example.domain.model.TvShow
import com.example.tvguide.R
import com.example.tvguide.utils.Utils

@Composable
fun TvShowCover(tvShow: TvShow) {
    Image(
        painter = if (tvShow.posterPath != null) rememberImagePainter(Utils.IMAGE_BASE_URL + tvShow.posterPath) else painterResource(id = R.drawable.no_image_camera),
        contentDescription = stringResource(R.string.season_thumbnail),
        contentScale = ContentScale.FillWidth,
        modifier = Modifier
            .height(200.dp)
            .fillMaxWidth()
            .padding(bottom = 12.dp)
    )

    Text(
        text = "Summary",
        color = MaterialTheme.colors.primaryVariant,
        fontSize = 16.sp,
        fontWeight = FontWeight.Bold,
        modifier = Modifier.padding(horizontal = 16.dp)
    )

    tvShow.overview?.let { Text(
        text = it,
        modifier = Modifier.padding(horizontal = 16.dp)
    ) }

    Spacer(modifier = Modifier.height(8.dp))
}