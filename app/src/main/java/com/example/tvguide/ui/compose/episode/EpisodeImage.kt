package com.example.tvguide.ui.compose.episode

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import coil.compose.rememberImagePainter
import com.example.tvguide.R
import com.example.tvguide.utils.Utils

@Composable
fun EpisodeImage(path: String?) {
    Image(
        painter = if (path != null) rememberImagePainter(Utils.IMAGE_BASE_URL + path) else painterResource(id = R.drawable.no_image_camera),
        contentDescription = stringResource(R.string.episode_thumbnail),
        contentScale = ContentScale.FillHeight,
        modifier = Modifier
            .height(100.dp)
            .width(100.dp)
    )
}