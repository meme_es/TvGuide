package com.example.tvguide.ui.compose.season

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import coil.compose.rememberImagePainter
import com.example.tvguide.R
import com.example.tvguide.utils.Utils

@Composable
fun SeasonImage(path: String?) {
    Image(
        painter = if (path != null) rememberImagePainter(Utils.IMAGE_BASE_URL + path) else painterResource(id = R.drawable.no_image),
        contentDescription = stringResource(R.string.season_thumbnail),
        contentScale = ContentScale.FillWidth,
        modifier = Modifier
            .height(125.dp)
            .width(100.dp)
    )
}