package com.example.tvguide.ui.compose.profile

import android.content.Intent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.content.ContextCompat
import com.example.domain.model.Account
import com.example.domain.model.TvShow
import com.example.tvguide.login.LoginActivity
import com.example.tvguide.profile.ProfileViewModel

@Composable
fun ComposeUserProfile(profileViewModel: ProfileViewModel, sessionId: String, accountId: Int) {
    profileViewModel.getFavoriteTvShows(accountId, sessionId)
    val tvShowList: List<TvShow> by profileViewModel.tvShowList.observeAsState(listOf())

    val profile: Account by profileViewModel.getUserAccountInfo(sessionId).observeAsState(Account())

    var showDialog = remember { mutableStateOf(false) }
    val context = LocalContext.current

    if (showDialog.value) {
        AlertDialog(
            onDismissRequest = { showDialog.value = false  },
            title = { Text(text = "Log out") },
            text = { Text(text = "Are you sure you want to leave?") },
            confirmButton = {
                Button(
                    onClick = {
                        showDialog.value = false
                        val intent = Intent(context, LoginActivity::class.java)
                        ContextCompat.startActivity(context, intent, null)
                    }) {
                    Text("Leave")
                }
            },
            dismissButton = {
                Button(
                    onClick = {
                        showDialog.value = false
                    }) {
                    Text("Stay")
                }
            },
        )
    }

    Column {
        TopAppBar(
            title = {
                Text(text = "Profile")
            }
        )

        UserProfile(profile)

        CompositionLocalProvider(LocalContentAlpha provides ContentAlpha.medium) {
            Text(
                text = "My Favorites",
                fontSize = 16.sp,
                modifier = Modifier.padding(horizontal = 16.dp),
                fontWeight = FontWeight.Bold
            )
        }

        LazyRow(
            contentPadding = PaddingValues(8.dp)
        ) {
            items(
                items = tvShowList,
                itemContent = {
                    TvShowItem(tvShow = it)
                }
            )
        }

        Button(
            onClick = { showDialog.value = true },
            modifier = Modifier
                .padding(16.dp)
                .fillMaxWidth()
        ) {
            Text("Log Out")
        }
    }
}